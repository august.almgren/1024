function reset(points) {
	for (let x = 0; x <= 6; x++) {
		for (let y = 0; y <= 6; y++) {
			let tile = document.getElementById(x + '-' + y);
			tile.parentElement.style.backgroundColor = 'white';
			tile.parentElement.style.color = 'black';
			tile.innerHTML = ' ';
		}
	}
	let tile = document.getElementById('3-3');
	tile.parentElement.style.backgroundColor = 'yellow';
	tile.innerHTML = points;
	document.getElementById('score').innerHTML = 0;
}

function update_tile_color(parent, col, row) {
	let tile = document.getElementById(col + '-' + row);
	if (col < 0 || col > 6 || row < 0 || row > 6 ||
			tile === null || tile.innerHTML === ' ' || tile.innerHTML === 'x'
			|| tile.parentElement.style.backgroundColor === 'gray') {
		return;
	}

	let left = col === 0 ? null : document.getElementById((col - 1) + '-' + row);
	let right = col === 6 ? null : document.getElementById((col + 1) + '-' + row);
	let up = row === 0 ? null : document.getElementById(col + '-' + (row - 1));
	let down = row === 6 ? null : document.getElementById(col + '-' + (row + 1));

	let left_open = left !== null && (left.innerHTML === tile.innerHTML
		|| left.innerHTML === ' ')
		&& left.parentElement.style.backgroundColor !== 'gray';
	let right_open = right !== null && (right.innerHTML === tile.innerHTML
		|| right.innerHTML === ' ')
		&& right.parentElement.style.backgroundColor !== 'gray';
	let up_open = up !== null && (up.innerHTML === tile.innerHTML
		|| up.innerHTML === ' ')
		&& up.parentElement.style.backgroundColor !== 'gray';
	let down_open = down !== null && (down.innerHTML === tile.innerHTML
		|| down.innerHTML === ' ')
		&& down.parentElement.style.backgroundColor !== 'gray';

	if (left_open || right_open || up_open || down_open) {
		tile.parentElement.style.backgroundColor = 'yellow';
	} else {
		tile.parentElement.style.backgroundColor = 'red';
	}
}

function remove_tile(parent, col, row) {
	if (col < 0 || col > 6 || row < 0 || row > 6) {
		return;
	}

	let tile = document.getElementById(col + '-' + row);
	if (tile === null || tile.innerHTML === ' ' || tile.parentElement.style.backgroundColor === 'gray') {
		return;
	}
	
	if (tile.innerHTML === 'x') {
		tile.parentElement.style.color = 'black';
	}

	tile.innerHTML = ' ';
	tile.parentElement.style.backgroundColor = 'white';
}

function block_tile(tile) {
	let points = parseInt(tile.innerHTML);
	let score = document.getElementById('score');
	score.innerHTML = parseInt(score.innerHTML) + points;
	tile.parentElement.style.backgroundColor = 'gray';
	tile.parentElement.style.color = 'white';
}

function move(col, row) {
	let tile = document.getElementById(col + '-' + row);
	let bgcolor = tile.parentElement.style.backgroundColor;
	let points = parseInt(document.getElementById(col + '-' + row).innerHTML);

	// only yellow tiles are clickable
	if (bgcolor !== 'yellow' && bgcolor !== 'red') {
		return;
	}

	if (bgcolor === 'red') {
		//tile.innerHTML = ' ';
		tile.parentElement.style.backgroundColor = 'white';

		remove_tile(parent, col - 1, row - 1);
		remove_tile(parent, col, row - 1);
		remove_tile(parent, col + 1, row - 1);
		remove_tile(parent, col - 1, row);
		block_tile(tile);
		remove_tile(parent, col + 1, row);
		remove_tile(parent, col - 1, row + 1);
		remove_tile(parent, col, row + 1);
		remove_tile(parent, col + 1, row + 1);

		// change colors of blocks adjacent to blast
		// top left
		let tl = document.getElementById((col - 1) + '-' + (row - 1));
		update_tile_color(tl, col - 2, row - 1);
		update_tile_color(tl, col - 1, row - 2);
		// top center
		let t = document.getElementById(col + '-' + (row - 1));
		update_tile_color(t, col, row - 2);
		// top right
		let tr = document.getElementById((col + 1) + '-' + (row - 1));
		update_tile_color(tr, col + 2, row - 1);
		update_tile_color(tr, col + 1, row - 2);
		// center left
		let l = document.getElementById((col - 1) + '-' + row);
		update_tile_color(l, col - 2, row);
		// center right
		let r = document.getElementById((col + 1) + '-' + row);
		update_tile_color(r, col + 2, row);
		// bottom left
		let bl = document.getElementById((col - 1) + '-' + (row + 1));
		update_tile_color(bl, col - 2, row + 1);
		update_tile_color(bl, col - 1, row + 2);
		// bottom center
		let b = document.getElementById(col + '-' + (row + 1));
		update_tile_color(b, col, row + 2);
		// bottom right
		let br = document.getElementById((col + 1) + '-' + (row + 1));
		update_tile_color(br, col + 2, row + 1);
		update_tile_color(br, col + 1, row + 2);

		return;
	}

	// kill tile
	tile.innerHTML = 'x';
	tile.parentElement.style.backgroundColor = 'black';
	tile.parentElement.style.color = 'white';

	// update neighboring tiles if necessary
	if (col !== 0) {
		let left = document.getElementById((col - 1) + '-' + row);
		if (left.parentElement.style.backgroundColor !== 'gray') {
			if (left.innerHTML === ' ') {
				left.innerHTML = points;
			} else if (parseInt(left.innerHTML) === points) {
				left.innerHTML = points * 2;
			}
			update_tile_color(tile, col - 2, row);
			update_tile_color(tile, col - 1, row);
			update_tile_color(tile, col - 1, row - 1);
			update_tile_color(tile, col - 1, row + 1);
		}
	}

	if (col !== 6) {
		let right = document.getElementById((col + 1) + '-' + row);
		if (right.parentElement.style.backgroundColor !== 'gray') {
			if (right.innerHTML === ' ') {
				right.innerHTML = points;
			} else if (parseInt(right.innerHTML) === points) {
				right.innerHTML = points * 2;
			}
			update_tile_color(tile, col + 2, row);
			update_tile_color(tile, col + 1, row);
			update_tile_color(tile, col + 1, row - 1);
			update_tile_color(tile, col + 1, row - 1);
		}
	}

	if (row !== 0) {
		let up = document.getElementById(col + '-' + (row - 1));
		if (up.parentElement.style.backgroundColor !== 'gray') {
			if (up.innerHTML === ' ') {
				up.innerHTML = points;
			} else if (parseInt(up.innerHTML) === points) {
				up.innerHTML = points * 2;
			}
			update_tile_color(tile, col, row - 2);
			update_tile_color(tile, col, row - 1);
			update_tile_color(tile, col - 1, row - 1);
			update_tile_color(tile, col + 1, row - 1);
		}
	}

	if (row !== 6) {
		let down = document.getElementById(col + '-' + (row + 1));
		if (down.parentElement.style.backgroundColor !== 'gray') {
			if (down.innerHTML === ' ') {
				down.innerHTML = points;
			} else if (parseInt(down.innerHTML) === points) {
				down.innerHTML = points * 2;
			}
			update_tile_color(tile, col, row + 2);
			update_tile_color(tile, col, row + 1);
			update_tile_color(tile, col - 1, row + 1);
			update_tile_color(tile, col + 1, row + 1);
		}
	}
}
